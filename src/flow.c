/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flow.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 17:37:03 by jayache           #+#    #+#             */
/*   Updated: 2019/08/18 17:37:04 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	**prepare_and_split(char **env, char *raw)
{
	char	*escaped;
	char	*expanded;
	char	**args;
	char	**cleaned;

	escaped = escape_string(raw);
	expanded = expand_var(env, escaped);
	args = get_args(expanded);
	cleaned = clean_escape(args);
	free(escaped);
	free(expanded);
	free_2d(args);
	return (cleaned);
}

int		handle_builtin(char ***env, char **command)
{
	if (!command || !command[0])
		return (0);
	if (!ft_strcmp(command[0], "env"))
		return (exec_env(*env, command));
	if (!ft_strcmp(command[0], "echo"))
		return (exec_echo(command));
	if (!ft_strcmp(command[0], "exit"))
		return (exec_exit());
	if (!ft_strcmp(command[0], "setenv"))
		return (exec_setenv(env, command));
	if (!ft_strcmp(command[0], "unsetenv"))
		return (exec_unsetenv(env, command));
	if (!ft_strcmp(command[0], "cd"))
		return (exec_cd(env, command));
	return (0);
}

void	exec_command(char ***env, char **command)
{
	char *name;

	if (handle_builtin(env, command))
		return ;
	if (command && command[0] && (command[0][0] == '.'
			|| command[0][0] == '/'))
		name = command[0];
	else
		name = find_bin(*env, command[0]);
	if (name && command)
		start_command(name, command, *env);
	else if (command[0])
		ft_printf("Command not found: %s\n", command[0]);
	free(name);
}
