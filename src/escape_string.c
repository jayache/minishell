/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   escape_string.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 17:41:53 by jayache           #+#    #+#             */
/*   Updated: 2019/08/18 18:10:48 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** Returns 1 if the character should be escaped
** Args:	char c: character to check
*/

static int	is_special(char c)
{
	return (c == ' ' || c == '\n' || c == '~');
}

/*
** Escape one char in buffer
** Args:	char *buffer: buffer into which we put the escaped char
**			int  pos: position into which we put the escaped char
**			char c: character to escape
*/

static int	escape_char(char *buffer, int pos, char c)
{
	buffer[pos] = '\\';
	buffer[pos + 1] = c;
	return (2);
}

static void	process_quote(char *current, char *result, char line, int *x)
{
	if (*current == 0)
		*current = line;
	else if (*current == line)
		*current = 0;
	else
	{
		result[*x] = line;
		(*x) += 1;
	}
}

/*
** Escape everything between two strings
** Args:	char *line: commandline
*/

char		*escape_string(char *line)
{
	int		i;
	int		x;
	char	current;
	char	*result;

	result = ft_strnew(ft_strlen(line) * 2);
	current = 0;
	i = 0;
	x = 0;
	while (line[i])
	{
		if ((line[i] == '"' || line[i] == '\'') && !is_escaped(line, i))
			process_quote(&current, result, line[i], &x);
		else if ((is_special(line[i])) && !is_escaped(line, i) && current)
			x += escape_char(result, x, line[i]);
		else
			result[x++] = line[i];
		++i;
	}
	ft_printf("%s\n", result);
	return (result);
}
