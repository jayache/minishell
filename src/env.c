/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 17:42:39 by jayache           #+#    #+#             */
/*   Updated: 2019/08/18 17:43:00 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** RETURN THE VALUE ASSOCIATED WITH NAME, NULL OTHERWISE
** Args:	char **env: environnement to check
** 			char *name: name of the env variable
*/

char			*return_var(char **env, char *name)
{
	int	i;

	i = 0;
	while (env[i])
	{
		if (ft_strnequ(env[i], name, ft_strlen(name)) && env[i])
			return (env[i] + ft_strlen(name) + 1);
		++i;
	}
	return (NULL);
}

static char		*free_and_return(char *result, char **paths)
{
	int	i;

	i = 0;
	while (paths[i])
	{
		free(paths[i]);
		++i;
	}
	free(paths);
	return (result);
}

/*
** RETURN THE NAME OF THE FIRST BINARY FOUND MATCHING NAME, NULL OTHERWISE
** Args:	char **env: environnement to check
**			char *name: command to search for
*/

char			*find_bin(char **env, char *name)
{
	int			i;
	char		*path;
	char		**paths;
	char		*fullpath;
	struct stat	buf;

	path = return_var(env, "PATH");
	if (!path || !name)
		return (NULL);
	paths = ft_strsplit(path, ':');
	i = 0;
	while (paths[i])
	{
		fullpath = ft_str3join(paths[i], "/", name);
		if (stat(fullpath, &buf) == 0)
			return (free_and_return(fullpath, paths));
		free(fullpath);
		++i;
	}
	return (free_and_return(NULL, paths));
}
