/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 17:37:40 by jayache           #+#    #+#             */
/*   Updated: 2019/08/18 17:40:33 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** Return a copy of the env
** ARGS:	char **env: environnement to copy
**			char *ignore: optional var skip
*/

char	**copy_env(char **env, char *ignore)
{
	int		i;
	int		x;
	char	**copy;

	x = 0;
	i = 0;
	while (env[i])
		++i;
	if (!ignore)
		++i;
	copy = (char**)malloc(sizeof(char*) * (i + 2));
	ft_bzero(copy, sizeof(char*) * (i + 2));
	i = 0;
	while (env[i])
	{
		if (!ignore || ft_strcmp(env[i], ignore))
			copy[x++] = ft_strdup(env[i]);
		++i;
	}
	return (copy);
}

/*
** free 2d array
** ARGS:	char **array: array to free
*/

void	free_2d(char **array)
{
	int	i;

	i = 0;
	while (array[i])
	{
		free(array[i]);
		++i;
	}
	free(array);
}

/*
** Check whether or not a character is escaped with backslashes or not
** Args:	char	*line: line to search into
**			int		pos: position of the character to check
*/

int		is_escaped(char *line, int pos)
{
	if (pos == 0 || (pos >= 1 && line[pos - 1] != '\\'))
		return (0);
	else if (pos > 1 && line[pos - 1] == '\\' && line[pos - 2] == '\\')
		return (0);
	else
		return (1);
}

/*
** Returns 1 if the char at pos is a non-escaped whitespace
** ARGS: 	char *line: line to search
**			int	 pos:	pos of the char
*/

int		is_space(char *line, int pos)
{
	return (!is_escaped(line, pos) && (line[pos] == ' '
			|| line[pos] == '\n' || line[pos] == '\t'
			|| line[pos] == '\0'));
}
