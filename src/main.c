/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/13 10:54:02 by jayache           #+#    #+#             */
/*   Updated: 2019/08/18 17:41:04 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern char **environ;

/*
** THIS FILES SERVE AS A TEMP TEST
*/

char	*prompt(char **env)
{
	char	*input;
	char	*user;

	user = return_var(env, "USERNAME");
	ft_printf("%{BLACK}%{green}$%{RED}%s%{BLACK}$>", user);
	get_next_line(1, &input);
	return (input);
}

void	interprete(char ***env, char *raw)
{
	char **cleaned;

	cleaned = prepare_and_split(*env, raw);
	exec_command(env, cleaned);
	free_2d(cleaned);
}

int		main(void)
{
	char	*raw;
	char	**env;

	env = copy_env(environ, NULL);
	while (1)
	{
		raw = prompt(env);
		interprete(&env, raw);
		free(raw);
	}
}
