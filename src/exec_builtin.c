/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_builtin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 17:32:48 by jayache           #+#    #+#             */
/*   Updated: 2019/08/18 17:35:51 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int			exec_setenv(char ***env, char **command)
{
	char	*var;
	char	**nenv;
	int		i;

	var = NULL;
	if (command[1])
		var = return_var(*env, command[1]);
	if (var)
		nenv = copy_env(*env, var - ft_strlen(command[1]) - 1);
	else
		nenv = copy_env(*env, var);
	i = 0;
	while (nenv[i])
		++i;
	nenv[i] = ft_str3join(command[1], "=", command[2]);
	free_2d(*env);
	*env = nenv;
	return (1);
}

int			exec_unsetenv(char ***env, char **command)
{
	char	*var;
	char	**nenv;

	var = NULL;
	if (command[1])
		var = return_var(*env, command[1]);
	if (var)
		nenv = copy_env(*env, var - ft_strlen(command[1]) - 1);
	else
		nenv = copy_env(*env, var);
	free_2d(*env);
	*env = nenv;
	return (1);
}

static void	update_oldpwd(char ***env, char *new_oldpwd)
{
	char **command;

	command = (char**)malloc(sizeof(char*) * 4);
	command[0] = "setenv";
	command[1] = "OLDPWD";
	command[2] = new_oldpwd;
	command[3] = 0;
	exec_setenv(env, command);
	free(command[2]);
	free(command);
}

int			exec_cd(char ***env, char **command)
{
	char	*dir;

	ft_printf("%s\n", command[1]);
	if (!command[1])
		dir = ft_strdup(return_var(*env, "HOME"));
	else if (command[1] && !ft_strcmp(command[1], "-"))
		dir = ft_strdup(return_var(*env, "OLDPWD"));
	else
		dir = ft_strdup(command[1]);
	if (dir)
	{
		update_oldpwd(env, getcwd(NULL, 0));
		chdir(dir);
	}
	free(dir);
	return (1);
}
