/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand_var.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 17:34:58 by jayache           #+#    #+#             */
/*   Updated: 2019/08/18 17:45:13 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** cut and insert in one function
** ARGS:	char **env: environnement
**			char *word: name of the var to expand
**			char *line: line to expand
**			int  pos:	position at which to expand
*/

static char		*insert_expand(char **env, char *word, char *line, int pos)
{
	char *cut;
	char *result;

	cut = ft_strcut(line, pos, pos + ft_strlen(word));
	result = ft_strins(cut, return_var(env, word), pos);
	free(cut);
	return (result);
}

/*
** expands tilde
** ARGS:
*/

static char		*expand_tilde(char **env, char *line)
{
	char	*nline;
	char	*nnline;
	int		i;

	nline = ft_strdup(line);
	i = 0;
	while (nline[i])
	{
		if (nline[i] == '~' && !is_escaped(nline, i)
				&& (i == 0 || is_space(nline, i - 1))
				&& (is_space(nline, i + 1)))
		{
			nnline = ft_strcut(nline, i, i);
			free(nline);
			nline = nnline;
			nnline = ft_strins(nline, return_var(env, "HOME"), i);
			free(nline);
			nline = nnline;
			i += ft_strlen(return_var(env, "HOME"));
		}
		else
			++i;
	}
	return (nline);
}

static char		*get_word(char *line, int pos)
{
	int		end;
	char	*result;

	end = pos;
	if (line[end])
		++end;
	while (line[end] && line[end] != ' ' && line[end] != '$'
			&& line[end] != '\\')
		++end;
	result = ft_strnew((end - pos) + 1);
	ft_strncpy(result, line + pos, end - pos);
	return (result + 1);
}

static char		*expand_dollar(char **env, char *line)
{
	char	*nline;
	char	*nnline;
	char	*word;
	int		i;

	nline = ft_strdup(line);
	i = 0;
	while (nline[i])
	{
		if (nline[i] == '$' && !is_escaped(nline, i))
		{
			word = get_word(nline, i);
			nnline = insert_expand(env, word, nline, i);
			free(nline);
			nline = nnline;
			i += ft_strlen(return_var(env, word));
			if (word)
				free(word - 1);
		}
		else
			++i;
	}
	return (nline);
}

/*
** expand variables from the line
** Args:	char	**env: environnement
**			char	*line: line to expand
*/

char			*expand_var(char **env, char *line)
{
	char	*nline;
	char	*result;

	nline = expand_tilde(env, line);
	result = expand_dollar(env, nline);
	free(nline);
	return (result);
}
