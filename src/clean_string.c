/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean_string.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 18:14:44 by jayache           #+#    #+#             */
/*   Updated: 2019/08/18 18:20:10 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** malloc a ** pointer of the size of array
** ARGS:		char **array: null terminated 2d array
*/

char		**malloc_2d(char **array)
{
	int	i;

	i = 0;
	while (array[i])
		++i;
	return ((char**)ft_memalloc(sizeof(char*) * (i + 1)));
}

/*
** Clean escapes in one argument
** ARGS: 	char *string: argument to escape
*/

static char	*escape_1d(char *string)
{
	int		i;
	int		x;
	char	*result;

	result = ft_strnew(ft_strlen(string));
	i = 0;
	x = 0;
	while (string[i])
	{
		if (string[i] != '\\')
		{
			result[x++] = string[i];
		}
		else if (string[i + 1] == '\\')
		{
			result[x++] = string[i];
		}
		++i;
	}
	return (result);
}

char		**clean_escape(char **args)
{
	int		i;
	char	**cleaned;

	i = 0;
	cleaned = malloc_2d(args);
	while (args[i])
	{
		cleaned[i] = escape_1d(args[i]);
		++i;
	}
	cleaned[i] = 0;
	return (cleaned);
}
