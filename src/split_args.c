/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 17:42:26 by jayache           #+#    #+#             */
/*   Updated: 2019/08/18 18:15:51 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** Get args from the call
** Args:	char	*line: command line
*/

char	**get_args(char *line)
{
	char	**args;
	char	*nline;
	int		i;

	i = 0;
	nline = ft_strdup(line);
	args = NULL;
	while (nline[i])
	{
		if (!is_escaped(nline, i) && (nline[i] == '\n'
					|| nline[i] == ' ' || nline[i] == '\t'))
			nline[i] = -90;
		++i;
	}
	args = ft_strsplit(nline, -90);
	free(nline);
	return (args);
}
