/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 17:37:14 by jayache           #+#    #+#             */
/*   Updated: 2019/08/18 17:41:40 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

/*
** Exec the builtin 'env'
** ARGS: 	char **env: environnement
*/

int		exec_env(char **env, char **command)
{
	int		i;
	pid_t	child;

	i = 0;
	if (command[1])
	{
		child = fork();
		if (child == 0)
		{
			exec_command(&env, command + 1);
			exit(0);
		}
		else if (child > 0)
		{
			wait(&i);
		}
		else
			ft_printf("Bof\n");
	}
	else
	{
		while (env[i])
			ft_printf("%s\n", env[i++]);
	}
	return (1);
}

/*
** Exec the builtin 'echo'
** ARGS:	char **command: arguments
*/

int		exec_echo(char **command)
{
	int	i;

	i = 1;
	while (command[i])
	{
		ft_printf("%s", command[i++]);
		if (command[i])
			ft_printf(" ");
	}
	ft_printf("\n");
	return (1);
}

/*
** Exec the builtin 'exit'
*/

int		exec_exit(void)
{
	exit(0);
	return (1);
}

/*
** Exec a normal command
** Args:	char	*fullpath: full path to the binary
**			char	**args: full list of arguments
**			char	**env: environnement
*/

void	start_command(char *fullpath, char **args, char **env)
{
	pid_t	pid;
	int		status;

	pid = fork();
	if (pid == 0)
	{
		signal(SIGINT, NULL);
		if (execve(fullpath, args, env) == -1)
		{
			ft_printf("bash: %s: No such file or directory\n", fullpath);
			execve("/bin/test", env, args);
			exit(0);
		}
		execve("/bin/test", env, args);
	}
	else
		wait(&status);
}
