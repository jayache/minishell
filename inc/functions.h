/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   functions.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jayache <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 18:20:54 by jayache           #+#    #+#             */
/*   Updated: 2019/08/18 18:20:55 by jayache          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FUNCTIONS_H
# define FUNCTIONS_H

char	*return_var(char **env, char *var);
char	*expand_var(char **env, char *line);
char	*find_bin(char **env, char *var);
char	**get_args(char *line);
char	*escape_string(char *raw);
char	**clean_escape(char **expanded);
char	**prepare_and_split(char **env, char *raw);
int		is_escaped(char *line, int pos);
int		is_space(char *line, int pos);
void	start_command(char *fullpath, char **arg, char **env);
void	free_2d(char **array);
void	exec_command(char ***env, char **command);
int		exec_env(char **env, char **command);
int		exec_echo(char **command);
int		exec_exit(void);
int		exec_setenv(char ***env, char **command);
int		exec_unsetenv(char ***env, char **command);
int		exec_cd(char ***env, char **command);
char	**copy_env(char **env, char *ignore);

#endif
