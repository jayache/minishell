# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jayache <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/17 14:14:36 by jayache           #+#    #+#              #
#    Updated: 2019/08/18 18:15:16 by jayache          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

MAIN = main.c
S = env.c exec.c expand_var.c escape_string.c split_args.c \
	clean_string.c helper.c flow.c exec_builtin.c
MO = $(MAIN:.c=.o)
O = $(S:.c=.o)
L = libft.a
I = minishell.h functions.h

TARGET = minishell
FLAGS = -Wextra -Wall -g3 #-fsanitize=address 

LIBDIR = lib/
OBJDIR = obj/
SRCDIR = src/
INCDIR = inc/

OBJ := $(addprefix $(OBJDIR), $(O))
MOBJ := $(addprefix $(OBJDIR), $(MO))
SRC := $(addprefix $(SRCDIR), $(S))
LIB := $(addprefix $(LIBDIR), $(L))
INC := $(addprefix $(INCDIR), $(I))
DEP = $(OBJ:%.o=%.d)

all:
	@mkdir -p $(OBJDIR)	
	@make -C $(LIBDIR) 
	@make $(TARGET)

$(OBJDIR)%.o: $(SRCDIR)%.c
	@mkdir -p $(OBJDIR)
	@gcc -MMD -c $(FLAGS) $< -I $(INCDIR) -o $@ 
	@echo "Creating $@"

$(TARGET): $(LIB) $(OBJ) $(MOBJ) $(INC)
	gcc $(OBJ) $(MOBJ) $(FLAGS) $(LIB) -o $(TARGET)

$(LIB):
	make -C $(LIBDIR)

clean:
	@rm -rf $(OBJ) $(UOBJ) $(MOBJ) $(DEP)
	@make clean -C $(LIBDIR) 

fclean: clean
	@rm -rf $(UTARGET) $(TARGET)
	@make fclean -C $(LIBDIR)

re: fclean all

-include $(DEP)
